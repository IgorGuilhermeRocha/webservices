package com.rocha.igor.webservices.Webservices.services.impl;

import com.rocha.igor.webservices.Webservices.models.Order;
import com.rocha.igor.webservices.Webservices.repositories.OrderRepository;
import com.rocha.igor.webservices.Webservices.services.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class OrderServiceImpl implements OrderService {

    @Autowired
    private OrderRepository orderRepository;

    @Override
    public List<Order> findAll() {
        return orderRepository.findAll();
    }

    @Override
    public Optional<Order> findById(Long id) {
        return orderRepository.findById(id);
    }
}

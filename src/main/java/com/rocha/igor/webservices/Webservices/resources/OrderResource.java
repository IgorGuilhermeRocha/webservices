package com.rocha.igor.webservices.Webservices.resources;

import com.rocha.igor.webservices.Webservices.models.Order;
import com.rocha.igor.webservices.Webservices.services.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping(value = "/orders")
public class OrderResource {

    @Autowired
    private OrderService orderService;


    @GetMapping
    public ResponseEntity<List<Order>> findAll(){
        List<Order> orders = orderService.findAll();
        if(orders.size() == 0) return ResponseEntity.noContent().build();
        return ResponseEntity.ok().body(orders);
    }

    @GetMapping(value = "{id}")
    public ResponseEntity<Order> findById(@PathVariable Long id){
        Optional<Order> optionalOrder = orderService.findById(id);
        if(!optionalOrder.isEmpty()) return ResponseEntity.ok().body(optionalOrder.get());
        return ResponseEntity.notFound().build();
    }
}

package com.rocha.igor.webservices.Webservices.resources;

import com.rocha.igor.webservices.Webservices.models.Category;
import com.rocha.igor.webservices.Webservices.services.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping(value = "/categories")
public class CategoryResource {

    @Autowired
    private CategoryService categoryService;


    @GetMapping
    public ResponseEntity<List<Category>> findAll(){
        List<Category> categories = categoryService.findAll();
        if(categories.size() == 0) return ResponseEntity.noContent().build();
        return ResponseEntity.ok().body(categories);
    }

    @GetMapping(value = "{id}")
    public ResponseEntity<Category> findById(@PathVariable Long id){
        Optional<Category> optionalCategory = categoryService.findById(id);
        if(!optionalCategory.isEmpty()) return ResponseEntity.ok().body(optionalCategory.get());
        return ResponseEntity.notFound().build();
    }
}

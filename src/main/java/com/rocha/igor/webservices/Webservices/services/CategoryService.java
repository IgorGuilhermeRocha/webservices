package com.rocha.igor.webservices.Webservices.services;
import com.rocha.igor.webservices.Webservices.models.Category;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public interface CategoryService {
    List<Category> findAll();

    Optional<Category> findById(Long id);
}

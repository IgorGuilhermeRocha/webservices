package com.rocha.igor.webservices.Webservices.services;
import com.rocha.igor.webservices.Webservices.models.Order;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public interface OrderService {
    List<Order> findAll();

    Optional<Order> findById(Long id);
}

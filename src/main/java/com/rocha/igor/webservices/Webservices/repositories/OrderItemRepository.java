package com.rocha.igor.webservices.Webservices.repositories;

import com.rocha.igor.webservices.Webservices.models.OrderItem;
import org.springframework.data.jpa.repository.JpaRepository;

public interface OrderItemRepository extends JpaRepository<OrderItem,Long> {
}

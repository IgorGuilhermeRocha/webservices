package com.rocha.igor.webservices.Webservices.repositories;

import com.rocha.igor.webservices.Webservices.models.Order;
import org.springframework.data.jpa.repository.JpaRepository;

public interface OrderRepository extends JpaRepository<Order,Long> {
}

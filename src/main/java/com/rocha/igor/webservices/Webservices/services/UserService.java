package com.rocha.igor.webservices.Webservices.services;
import com.rocha.igor.webservices.Webservices.models.User;
import org.springframework.stereotype.Service;
import java.util.List;
import java.util.Optional;

@Service
public interface UserService {
    List<User> findAll();
    User findById(Long id);
    Optional<User> insert(User user);
    void delete(Long id);
    User update(Long id, User user);


}

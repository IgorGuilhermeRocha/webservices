package com.rocha.igor.webservices.Webservices.repositories;

import com.rocha.igor.webservices.Webservices.models.Product;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ProductRepository  extends JpaRepository<Product,Long> {
}

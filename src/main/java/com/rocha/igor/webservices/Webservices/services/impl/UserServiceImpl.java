package com.rocha.igor.webservices.Webservices.services.impl;

import com.rocha.igor.webservices.Webservices.models.User;
import com.rocha.igor.webservices.Webservices.repositories.UserRepository;
import com.rocha.igor.webservices.Webservices.services.UserService;
import com.rocha.igor.webservices.Webservices.services.exceptions.DatabaseException;
import com.rocha.igor.webservices.Webservices.services.exceptions.ResourceNotFoundException;
import org.hibernate.exception.ConstraintViolationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import java.util.List;
import java.util.Optional;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserRepository userRepository;

    @Override
    public List<User> findAll() {
        return userRepository.findAll();
    }

    @Override
    public User findById(Long id) {
      return userRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException(id));
    }

    @Override
    public Optional<User> insert(User user) {
        return Optional.ofNullable(userRepository.save(user));
    }

    @Override
    public void delete(Long id) {
        try{
            userRepository.deleteById(id);
        }catch (EmptyResultDataAccessException e){
            throw new ResourceNotFoundException(id);
        }catch (DataIntegrityViolationException e){
            throw new DatabaseException(e.getMessage());
        }
    }

    @Override
    public User update(Long id, User user) {
        try{
            User entity = userRepository.getReferenceById(id);
            updateData(entity, user);
            return userRepository.save(entity);
        }catch (EntityNotFoundException e){
            throw new ResourceNotFoundException(id);
        }
    }

    private void updateData(User entity, User user){
            entity.setEmail(user.getEmail());
            entity.setName(user.getName());
            entity.setPhone(user.getPhone());
    }


}

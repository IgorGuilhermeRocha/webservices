package com.rocha.igor.webservices.Webservices.services.impl;
import com.rocha.igor.webservices.Webservices.models.Category;
import com.rocha.igor.webservices.Webservices.repositories.CategoryRepository;
import com.rocha.igor.webservices.Webservices.services.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class CategoryServiceImpl implements CategoryService {

    @Autowired
    private CategoryRepository categoryRepository;

    @Override
    public List<Category> findAll() {
        return categoryRepository.findAll();
    }

    @Override
    public Optional<Category> findById(Long id) {
        return categoryRepository.findById(id);
    }
}

package com.rocha.igor.webservices.Webservices.repositories;

import com.rocha.igor.webservices.Webservices.models.Category;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CategoryRepository extends JpaRepository<Category,Long> {
}

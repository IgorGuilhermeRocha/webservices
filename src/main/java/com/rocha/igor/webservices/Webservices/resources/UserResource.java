package com.rocha.igor.webservices.Webservices.resources;

import com.rocha.igor.webservices.Webservices.models.User;
import com.rocha.igor.webservices.Webservices.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping(value = "/users")
public class UserResource {

    @Autowired
    private UserService userService;


    @GetMapping
    public ResponseEntity<List<User>> findAll(){
        List<User> users = userService.findAll();
        if(users.size() == 0) return ResponseEntity.noContent().build();
        return ResponseEntity.ok().body(users);
    }

    @GetMapping(value = "{id}")
    public ResponseEntity<User> findById(@PathVariable Long id){
        return ResponseEntity.ok().body(userService.findById(id));
    }

    @PostMapping()
    public ResponseEntity<User> insert(@RequestBody User user){
        Optional<User> optionalUser = userService.insert(user);
        URI uri = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}").buildAndExpand(optionalUser.get().getId()).toUri();
        if(!optionalUser.isEmpty()) return ResponseEntity.created(uri).body(optionalUser.get());
        return ResponseEntity.badRequest().build();
    }
    @DeleteMapping("{id}")
    public ResponseEntity<Void> delete(@PathVariable Long id){
        userService.delete(id);
        return ResponseEntity.noContent().build();
    }
    @PutMapping("{id}")
    public ResponseEntity update(@PathVariable Long id, @RequestBody User user){
        User responseUser = userService.update(id, user);
        return ResponseEntity.ok().body(responseUser);
    }

}

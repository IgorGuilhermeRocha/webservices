package com.rocha.igor.webservices.Webservices.resources;

import com.rocha.igor.webservices.Webservices.models.Product;
import com.rocha.igor.webservices.Webservices.services.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/products")
public class ProductResource {

    @Autowired
    private ProductService productService;


    @GetMapping
    public ResponseEntity<List<Product>> findAll(){
        List<Product> orders = productService.findAll();
        if(orders.size() == 0) return ResponseEntity.noContent().build();
        return ResponseEntity.ok().body(orders);
    }

    @GetMapping(value = "{id}")
    public ResponseEntity<Product> findById(@PathVariable Long id){
        Optional<Product> product = productService.findById(id);
        if(!product.isEmpty()) return ResponseEntity.ok().body(product.get());
        return ResponseEntity.notFound().build();
    }

}

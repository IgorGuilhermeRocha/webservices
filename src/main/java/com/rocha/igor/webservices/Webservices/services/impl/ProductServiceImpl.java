package com.rocha.igor.webservices.Webservices.services.impl;
import com.rocha.igor.webservices.Webservices.models.Product;
import com.rocha.igor.webservices.Webservices.repositories.ProductRepository;
import com.rocha.igor.webservices.Webservices.services.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ProductServiceImpl implements ProductService {

    @Autowired
    private ProductRepository productRepository;

    @Override
    public List<Product> findAll() {
        return productRepository.findAll();
    }

    @Override
    public Optional<Product> findById(Long id) {
        return productRepository.findById(id);
    }
}

package com.rocha.igor.webservices.Webservices.models;

public enum OrderStatus {
    WAITING_PAYMENT(1),
    PAID(2),
    SHIPPED(3),
    DELIVERED(4),
    CANCELED(5);

    private Integer code;
    private OrderStatus(int code){
        this.code = code;
    }

    public Integer getCode(){ return this.code; }

    public static OrderStatus getStatusByCode(int code){
        for(OrderStatus status : OrderStatus.values()){
            if(status.getCode() == code) return status;
        }
        throw new IllegalArgumentException("Code do not exist");
    }

}

package com.rocha.igor.webservices.Webservices.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.rocha.igor.webservices.Webservices.models.pk.OrderItemPk;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.util.Objects;

@Entity
@Table(name = "tb_order_item")
public class OrderItem {

    @EmbeddedId
    private OrderItemPk id =  new OrderItemPk();
    private Double price;
    private Integer quantity;

    public OrderItem(){

    }

    public OrderItem(Order order, Product product, Integer quantity, Double price){
        this.id.setOrder(order);
        this.id.setProduct(product);
        this.price = price;
        this.quantity = quantity;

    }


    public Product getProduct(){
        return this.id.getProduct();
    }

    public void setProduct(Product product){
        this.id.setProduct(product);
    }

    @JsonIgnore
    public Order getOrder(){
        return this.id.getOrder();
    }
    public void setOrder(Order order){
        this.id.setOrder(order);
    }
    public Double getPrice() {
        return price;
    }
    public void setPrice(Double price) {
        this.price = price;
    }

    public Integer getQuantity() {
        return quantity;
    }
    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public Double getSubTotal(){
        return this.price * this.quantity;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        OrderItem orderItem = (OrderItem) o;
        return Objects.equals(id, orderItem.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}

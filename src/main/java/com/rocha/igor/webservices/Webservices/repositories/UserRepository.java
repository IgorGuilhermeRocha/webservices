package com.rocha.igor.webservices.Webservices.repositories;
import com.rocha.igor.webservices.Webservices.models.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends JpaRepository<User,Long> {


}

package com.rocha.igor.webservices.Webservices.services;
import com.rocha.igor.webservices.Webservices.models.Product;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public interface ProductService {
    List<Product> findAll();

    Optional<Product> findById(Long id);
}

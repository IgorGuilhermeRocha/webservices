<h1> Projeto WebServices</h1>  
<p> É um projeto feito para simular um ambiente de compras de produtos em uma loja online.</p>
<p> Se quiser, poderá clonar o projeto e com o postman instalado acessar alguns endpoints:</p>

<ul>
    <li>/users</li>
    <li>/orders</li>
    <li>/categories</li>
    <li>/products</li>
</ul>

<h2>Stacks:</h2>
<ul>
    <li>Spring Framework 2.7.5</li>
    <li>Spring data Jpa 2.7.5</li>
    <li>Java 17</li>
    <li>H2 database</li>
</ul>
